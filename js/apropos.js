function send_(){
    var selectForm = document.querySelector("#formID");
    var tableau = document.querySelector("#tableID");

    let name = selectForm.name.value;
    let firstname = selectForm.firstname.value;
    let email = selectForm.email.value;
    let message = selectForm.t3.value
    let gem = selectForm.gem.value;

    if (name && email && message && gem) {
        let row = document.createElement("tr");

        let colName = document.createElement("td");
        colName.textContent = name;
        row.append(colName);

        let colFirstname = document.createElement("td");
        colFirstname.textContent = firstname;
        row.append(colFirstname);

        let colAddress = document.createElement("td");
        colAddress.textContent = email;
        row.append(colAddress);

        let colMode = document.createElement("td");
        colMode.textContent = gem;
        row.append(colMode);

        let colEntree = document.createElement("td");
        colEntree.textContent = message;
        row.append(colEntree);

        tableau.appendChild(row);
    }
    else alert("Le formulaire est incomplet !");
}

function clear_(){
    var tableau = document.querySelector("#tableID");

    while (tableau.children.length > 1) {
        tableau.removeChild(tableau.children[tableau.children.length - 1]);
    }

}
