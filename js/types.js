function click_donnees(){
    let b_donnees = document.querySelector("#button_donnees");
    let b_explica = document.querySelector("#button_explica");
    let d_donnees = document.querySelector("#data_donnees");
    let d_explica = document.querySelector("#data_explica");
    b_donnees.style.backgroundColor = "rgba(150,200,150,0.6)";
    b_explica.style.background = "none";
    d_donnees.style.display = "block";
    d_explica.style.display = "none";
    d_donnees.style.backgroundColor = "rgba(150,200,150,0.6)";
    d_explica.style.background = "none";
}

function click_explica(){
    let b_donnees = document.querySelector("#button_donnees");
    let b_explica = document.querySelector("#button_explica");
    let d_donnees = document.querySelector("#data_donnees");
    let d_explica = document.querySelector("#data_explica");
    b_donnees.style.background = "none";
    b_explica.style.backgroundColor = "rgba(200,150,150,0.6)";
    d_donnees.style.display = "none";
    d_explica.style.display = "block";
    d_donnees.style.background = "none";
    d_explica.style.backgroundColor = "rgba(200,150,150,0.6)";
}