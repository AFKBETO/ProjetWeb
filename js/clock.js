function clock() {
    let d = new Date();
    let hr = d.getHours();
    let min = d.getMinutes();
    let sec = d.getSeconds();
    let hr_angle = 180 + 30 * (hr + min/60 + sec/3600);
    let min_angle = 180 + 6 * (min + sec/60);
    let sec_angle = 180 + 6 * sec;
    let hour = document.querySelector('.hour');
    let minute = document.querySelector('.minute');
    let second = document.querySelector('.second');

    hour.style.transform = `rotate(${hr_angle}deg)`;
    minute.style.transform = `rotate(${min_angle}deg)`;
    second.style.transform = `rotate(${sec_angle}deg)`;
}

setInterval(clock, 1000);